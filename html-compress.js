var mini = require('html-minifier');
process.stdin.resume()
process.stdin.on('data', function(data) {
	process.stdout.write(mini.minify(String(data), {
		collapseBooleanAttributes:true,
		collapseWhitespace:true,
		removeAttributeQuotes:true,
		removeCDATASectionsFromCDATA:true,
		removeComments:true,
		removeCommentsFromCDATA:true,
		removeEmptyAttributes:true,
		removeOptionalTags:true,
		removeRedundantAttributes:true,
		removeScriptTypeAttributes:true,
		removeStyleLinkTypeAttributes:true,
		useShortDoctype:true
	}));
});
process.stdout.on('error', function(err) {
  if (err.code === 'EPIPE') return process.exit()
  process.emit('error', err)
})
