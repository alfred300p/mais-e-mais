(function() {
	function next_thumb() {
		$('li.hidden:first', $thumbs_ul).remove().removeClass('hidden').appendTo($thumbs_ul);
		$('li:first', $thumbs_ul).addClass('hidden');
		thumb_timeout = setTimeout(next_thumb, 4000)
	}

	$('a.email').each(function() {
		var email = 'tp.siamesiam@rebas'.split("").reverse().join("");
		var $this = $(this);
		$this.attr('href', 'mailto:' + email);
		if ($this.is('.text')) {
			$this.text(email);
		}
	});
	$('.toggle').on('click', function() {
		var $this = $(this);
		$this.toggleClass('visible');
		$('#' + $this.attr('for')).toggle();
	});

	$tds = $('.pricetable td').filter(function() {
		return $(this).text().match(/^[0-9]+€$/);
	});
	$('td').filter(function() {
		return $(this).text().match(/^[0-9]+€$/);
	}).map(function() {
		return parseInt($(this).text());
	})
	$tds.each(function() {
		var $this = $(this);
		var text = $this.text();
		$this.empty();
		$('<span class="normal-price"/>').text(text).appendTo($this);
		$('<span class="discount-price"/>').text((parseInt(text) * 0.9).toFixed(1) + '€').appendTo($this);
	});
	$('.pointer').on('click', function() {
		$('body').toggleClass('discount-mode');
	});

	$('#map-link').on('click', function() {
		var $map = $('iframe.map');
		$map.attr('src', $map.data('src')).show();
		$('#map-sep').hide();
	})

	var thumb_timeout = setTimeout(next_thumb, 5000);
	var $thumbs_ul = $('.thumbs ul').on('click', 'li', function() {
		clearTimeout(thumb_timeout);
		next_thumb();
	});
})();