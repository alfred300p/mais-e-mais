
DEST:=dest
SOURCE_JS:=main.js
DEST_JS:=${DEST}/saber.js
SOURCE_CSS:=main.css
DEST_CSS:=${DEST}/saber.css
SOURCE_HTML:=index.html
DEST_HTML:=${DEST}/index.html
SOURCE_PNG:=logo.png mascot.png favicon.png
DEST_IMAGES:=$(addprefix ${DEST}/,${SOURCE_PNG})
SOURCE_THUMBS:=$(wildcard thumbs/*.jpg)
DEST_THUMBS:=$(addprefix ${DEST}/,${SOURCE_THUMBS})
REMOTE_USER:=maisemais
REMOTE_HOST:=cod.pt
REMOTE_DIR:=/home/maisemais/site

help:
	@echo "make watch:   watches for changed, builds when necessary"
	@echo "make build:   builds destination files on ${DEST}/"
	@echo "make clean:   cleans all built files"
	@echo "make install: builds and installs to the server"
	@echo "make ssh:     opens a shell to the server"

build: ${DEST} ${DEST_JS} ${DEST_CSS} ${DEST_HTML} ${DEST_IMAGES} ${DEST_THUMBS}

clean:
	rm -rf ${DEST}

watch:
	@while true; do sleep 1; make build | grep -v 'Nothing to be done'; done

ssh:
	ssh ${REMOTE_USER}@${REMOTE_HOST}

install: clean build
	rsync --delete -av ${DEST}/ ${REMOTE_USER}@${REMOTE_HOST}:${REMOTE_DIR}

${DEST}:
	mkdir -p ${DEST} ${DEST}/thumbs

${DEST_JS}: ${SOURCE_JS}
	cat ${SOURCE_JS} | yuicompressor --type js > ${DEST_JS}

${DEST_CSS}: ${SOURCE_CSS}
	cat ${SOURCE_CSS} | yuicompressor --type css > ${DEST_CSS}

${DEST_HTML}: ${SOURCE_HTML}
	cat ${SOURCE_HTML} | node html-compress.js > ${DEST_HTML}

${DEST}/%.png: %.png
	cp $(notdir $@) $@

${DEST_THUMBS}:
	cp thumbs/$(notdir $@) $@
